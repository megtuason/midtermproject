class CarLog < ActiveRecord::Base
	validates :plate_number, presence: true, length: { maximum: 8 }
	self.table_name = "carlogs"
end
