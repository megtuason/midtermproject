class CarLogsController < ApplicationController
	def home
		@carlogslast = CarLog.last
		if @carlogslast = nil
			flash[:notice] = "No existing car logs"
			redirect_to car_log_home_path
		else
			render 'new'
		end
	end
	def about
		@carlogslast = CarLog.last
		
	end
	def new
		@carlogs = CarLog.new
		@carlogslast = CarLog.last
	end
	def create
		@carlogs = CarLog.new(carlogs_params)
		if @carlogs.save
			flash[:notice] = "Created New Car Log"
			redirect_to car_log_path(@carlogs)
		else
			render 'new'
		end
	end
	def show
		@carlogs = CarLog.last
	end
	private
	def carlogs_params
		params.require(:car_log).permit(:plate_number, :time_in, :time_out, :color, :brand, :created_at, :updated_at)
	end
end
