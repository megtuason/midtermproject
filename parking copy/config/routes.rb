Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'car_logs/home', to: 'car_logs#home'
  get 'car_logs/about', to: 'car_logs#about'
  resources :car_logs
  root to:'car_logs#home'
end
